Tor's Gitlab Lobby
==================

This Django application contains the lobby website for Tor's Gitlab instance.
The Gitlab Lobby allows users to:

1. Request accounts on our Gitlab server, if they are interested in working
   with Tor's development teams.

2. Anonymously submit and comment on issues on Tor's Gitlab instance.


# To run it locally

```
$ virtualenv -p python3.7 .env
$ source .env/bin/activate
$ pip install -r requirements.txt
$ python src/manage.py makemigrations
$ python src/manage.py migrate
$ python src/manage.py runserver
```

