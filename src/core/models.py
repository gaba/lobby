from django.db import models
from django import forms


class GitlabAccountRequest(models.Model):
    username = models.CharField(max_length=64)
    email    = models.EmailField()
    approved = models.BooleanField(default=False)

class GitlabAccountRequestForm(forms.ModelForm):
    class Meta:
        model = GitlabAccountRequest
        fields = ["username", "email"]
