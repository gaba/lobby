import os.path
import secrets

from typing import List

_wordlist = None

def read_wordlist() -> List[str]:
    global _wordlist

    if _wordlist is None:
        with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "data", "en.txt")) as f:
            _wordlist = f.read().splitlines()

    return _wordlist

def random(n: int) -> List[str]:
    assert n >= 0

    result = []
    words = read_wordlist()

    for _ in range(0, n):
        result.append(secrets.choice(words))

    return result
