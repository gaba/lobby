import gitlab

from django.contrib import admin

from .models import GitlabAccountRequest

from lobby.settings import GITLAB_URL, GITLAB_SECRET_TOKEN


def approve(modeladmin, request, queryset):
    for item in queryset:
        client = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_SECRET_TOKEN)

        try:
            client.users.create({
                "name":              item.username,
                "username":          item.username,
                "email":             item.email,
                "reset_password":    True,
                "projects_limit":    5,
                "can_create_group":  False,
                "skip_confirmation": True, # The password reset mail is enough.
            })
        except Exception as e:
            print("Error: {}".format(e))
            continue

        item.approved = True
        item.save()

approve.short_description = "Approve account requests"

def delete(modeladmin, request, queryset):
    for item in queryset:
        item.delete()

delete.short_description = "Delete account requests"

@admin.register(GitlabAccountRequest)
class GitlabAccountRequestAdmin(admin.ModelAdmin):
    fields = ('username', 'email', 'approved')
    list_display = ('username', 'email', 'approved')
    actions = [approve, delete]

    def has_delete_permission(self, request, obj=None):
        return False
