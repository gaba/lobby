from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import GitlabAccountRequestForm

def index(request):
    if request.method == "POST":
        form = GitlabAccountRequestForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/success/')

        return HttpResponseRedirect('/failure/')

    return render(request, 'core/signup.html')

def success(request):
    return render(request, 'core/success.html')

def failure(request):
    return render(request, 'core/failure.html')
